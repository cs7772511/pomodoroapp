﻿namespace PomodoroApp
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components=new System.ComponentModel.Container();
            btnStart=new Button();
            btnReset=new Button();
            tbWork=new TextBox();
            tbRest=new TextBox();
            lblDisplay=new Label();
            tmrTime=new System.Windows.Forms.Timer(components);
            SuspendLayout();
            // 
            // btnStart
            // 
            btnStart.Location=new Point(182, 241);
            btnStart.Name="btnStart";
            btnStart.Size=new Size(103, 45);
            btnStart.TabIndex=0;
            btnStart.Text="START";
            btnStart.UseVisualStyleBackColor=true;
            btnStart.Click+=btnStart_Click;
            // 
            // btnReset
            // 
            btnReset.Location=new Point(487, 241);
            btnReset.Name="btnReset";
            btnReset.Size=new Size(103, 45);
            btnReset.TabIndex=1;
            btnReset.Text="RESET";
            btnReset.UseVisualStyleBackColor=true;
            btnReset.Click+=btnReset_Click;
            // 
            // tbWork
            // 
            tbWork.Location=new Point(185, 119);
            tbWork.Name="tbWork";
            tbWork.Size=new Size(100, 23);
            tbWork.TabIndex=2;
            tbWork.TextChanged+=tbWork_TextChanged;
            // 
            // tbRest
            // 
            tbRest.Location=new Point(487, 119);
            tbRest.Name="tbRest";
            tbRest.Size=new Size(100, 23);
            tbRest.TabIndex=3;
            // 
            // lblDisplay
            // 
            lblDisplay.AutoSize=true;
            lblDisplay.BackColor=SystemColors.MenuText;
            lblDisplay.Font=new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            lblDisplay.ForeColor=SystemColors.ButtonHighlight;
            lblDisplay.Location=new Point(378, 193);
            lblDisplay.Name="lblDisplay";
            lblDisplay.Size=new Size(22, 25);
            lblDisplay.TabIndex=4;
            lblDisplay.Text="0\r\n";
            // 
            // tmrTime
            // 
            tmrTime.Interval=10;
            tmrTime.Tick+=tmrTime_Tick;
            // 
            // FormMain
            // 
            AutoScaleDimensions=new SizeF(7F, 15F);
            AutoScaleMode=AutoScaleMode.Font;
            ClientSize=new Size(800, 450);
            Controls.Add(lblDisplay);
            Controls.Add(tbRest);
            Controls.Add(tbWork);
            Controls.Add(btnReset);
            Controls.Add(btnStart);
            Name="FormMain";
            Text="Pomodoro App";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnStart;
        private Button btnReset;
        private TextBox tbWork;
        private TextBox tbRest;
        private Label lblDisplay;
        private System.Windows.Forms.Timer tmrTime;
    }
}