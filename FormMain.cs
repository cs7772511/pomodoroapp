namespace PomodoroApp
{
    public partial class FormMain : Form
    {
        private int numberOfSeconds;
        private bool currentlyWorking;

        private void UpdateTimerDisplay()
        {
            lblDisplay.Text = string.Format("{0}: {1}", numberOfSeconds/60, numberOfSeconds%60);
        }

        public FormMain()
        {
            InitializeComponent();
            numberOfSeconds = 25*60;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrTime.Enabled = true;
            currentlyWorking = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            tmrTime.Enabled = false;
            currentlyWorking = false;
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            numberOfSeconds--;
            if (numberOfSeconds == 0)
            {
                if (currentlyWorking)
                {
                    numberOfSeconds = 5*60;
                }
                else
                {
                    numberOfSeconds = 25*60;
                }
                currentlyWorking = !currentlyWorking;
            }
            UpdateTimerDisplay();
        }

        private void tbWork_TextChanged(object sender, EventArgs e)
        {

        }
    }
}